using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    private Rigidbody rigidbody;
    public float speed = 2f;
    bool canJump = true;
    float jumpCooldown = 1;
    

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetKey("w")){
            rigidbody.AddForce(200, 0, 0 * Time.deltaTime);
        }
        if(Input.GetKey("s")){
            rigidbody.AddForce(-200, 0, 0 * Time.deltaTime);
        }
    }

    void Update()
    {
        if(Input.GetKey("space") & canJump){
            rigidbody.AddForce(0, 2000, 0 * Time.deltaTime);
            StartCoroutine(CoolDownFunction());
        }
    }

    IEnumerator CoolDownFunction()
    {
        canJump = false;
        yield return new WaitForSeconds(jumpCooldown);
        canJump = true;
    }
}
